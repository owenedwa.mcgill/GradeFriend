﻿using SQLite;
using System.Collections.Generic;
using System.Linq;
using SQLiteNetExtensions.Extensions;
using System;

namespace GradeFriend
{
    public class CourseHelper
    {
        SQLiteConnection Connection;

        public CourseHelper(SQLiteConnection connection)
        {
            Connection = connection;
            FlushTable();
            Connection.CreateTable<Course>();
        }

        public bool FlushTable()
        {
            try
            {
                Connection.Execute("delete from " + CONSTANTS.TABLE_COURSE);
                Connection.Execute("delete from sqlite_sequence where name = '" + CONSTANTS.TABLE_COURSE + "'");
            }
            catch (SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public bool AddCourse(string courseName, string teacherName, int studentId, List<Grade> grades)
        {
            try
            {
                Course _course = new Course()
                {
                    Name = courseName,
                    Teacher = teacherName,
                    StudentId = studentId,
                    Grades = grades
                };

                Connection.InsertWithChildren(_course);
            }
            catch (SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public List<Course> GetCourseList(Student student)
        {
            return Connection.GetWithChildren<Student>(student.Id).Courses;
        }

        public List<Course> GetCourseList()
        {
            return Connection.Table<Course>().ToList();
        }
    }
}
