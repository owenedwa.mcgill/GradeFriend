﻿using System;
using System.Collections.Generic;

namespace GradeFriend
{
    public class JSONModel
    {
        public List<JSONStudent> students { get; set; }
    }

    public class JSONStudent
    {
        public string student_name { get; set; }
        public string student_id { get; set; }
        public List<JSONCourse> courses { get; set; }
    }

    public class JSONCourse
    {
        public string course { get; set; }
        public string teacher { get; set; }
        public List<JSONGrade> grades { get; set; }
    }

    public class JSONGrade
    {
        public string description { get; set; }
        public decimal weight { get; set; }
        public decimal mark { get; set; }
    }
}
