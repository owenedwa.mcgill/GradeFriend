﻿using System;
namespace GradeFriend
{
    public static class CONSTANTS
    {
        public const string DATABASE_NAME = "GradeFriend.db3";
        public const string TABLE_STUDENT = "student";
        public const string TABLE_COURSE = "course";
        public const string TABLE_GRADE = "grade";

        public const string JSON_STUDENTS_KEY = "students";
        public const string JSON_STUDENT_NAME_KEY = "student_name";
        public const string JSON_STUDENT_ID_KEY = "student_id";
        public const string JSON_COURSES_KEY = "courses";
        public const string JSON_COURSE_KEY = "course";
        public const string JSON_TEACHER_KEY = "teacher";
        public const string JSON_GRADES_KEY = "grade";
        public const string JSON_DESCRIPTION_KEY = "description";
        public const string JSON_WEIGHT_KEY = "weight";
        public const string JSON_MARK_KEY = "mark";
        public const string TEACHER_CODE = "1848LIN-HEIAFR";

        public const string STUDENT_ID = "StudentID";

        public const string DATA_URI = "https://gradefriend-26cb5.firebaseio.com/data.json";
    }
}
