﻿using SQLite;
using System;
using SQLiteNetExtensions.Attributes;

namespace GradeFriend
{
    [Table("Grade")]
    public class Grade
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public decimal Mark { get; set; }
        public decimal Weight { get; set; }
        public string Description { get; set; }

        [ForeignKey(typeof(Course))]
        public int CourseID { get; set; }

    }
}
