﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GradeFriend
{
    public class MyModel
    {

        private DBHelper DB;

        private GradeHelper Grades;

        private CourseHelper Courses;

        private StudentHelper Students;

        public string StudentId { get; set; }

        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        public List<Course> ListCourse { get; set; }

        public Course SelectedCourse { get; set; }

        protected MyModel(string path)
        {
            DB = DBHelper.GetInstance(path);
            var connection = DB.GetDBConnection();
            Grades = new GradeHelper(connection);
            Courses = new CourseHelper(connection);
            Students = new StudentHelper(connection);
            InitModel();
        }

        private void InitModel() {
            ListCourse = new List<Course>();
            SelectedCourse = null;
        }

        private void FlushModel() {
            Grades.FlushTable();
            Courses.FlushTable();
            Students.FlushTable();
        }

        public async Task<bool> GetModelDataTask() {

            await semaphoreSlim.WaitAsync();
            FlushModel();

            using (HttpClient client = new HttpClient()) {
                string _jsonData = await client.GetStringAsync(CONSTANTS.DATA_URI);

                JSONModel data = JsonConvert.DeserializeObject<JSONModel>(_jsonData);

                var _jStudents = data.students;
                var _students = new List<Student>();
                foreach (JSONStudent _jStudent in _jStudents)
                {
                    var _jCourses = _jStudent.courses;
                    var _courses = new List<Course>();
                    foreach (JSONCourse _jCourse in _jCourses)
                    {
                        var _jGrades = _jCourse.grades;
                        var _grades = new List<Grade>();
                        foreach (JSONGrade _jGrade in _jGrades)
                        {
                            var _grade = new Grade()
                            {
                                Description = _jGrade.description,
                                Mark = _jGrade.mark,
                                Weight = _jGrade.weight
                            };
                            _grades.Add(_grade);
                        }
                        var _course = new Course()
                        {
                            Teacher = _jCourse.teacher,
                            Name = _jCourse.course,
                            Grades = _grades
                        };
                        _courses.Add(_course);
                    }
                    Student _student = new Student()
                    {
                        Name = _jStudent.student_name,
                        StudentId = _jStudent.student_id,
                        Courses = _courses
                    };
                    _students.Add(_student);
                }

                Students.AddStudents(_students);

            }
            InitModel();
            ListCourse = Students.GetStudent(StudentId).Courses;
            semaphoreSlim.Release();
            return true;
        }

        public List<Grade> GetGrades(Course course)
        {
            return Grades.GetGrades(course);
        }
    }
}
