﻿using SQLite;
using System;
using System.Collections.Generic;
using SQLiteNetExtensions.Attributes;

namespace GradeFriend
{
    [Table("Course")]
    public class Course
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Teacher { get; set; }

        [ForeignKey(typeof(Student))]
        public int StudentId { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Grade> Grades { get; set; }


    }
}
