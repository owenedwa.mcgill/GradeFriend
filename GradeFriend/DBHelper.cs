﻿using System;
using SQLite;

namespace GradeFriend
{
    public class DBHelper
    {
        public const string DataBaseName = CONSTANTS.DATABASE_NAME;

        private SQLiteConnection Connection { get; }
        private static DBHelper _Instance;

        private DBHelper(string path)
        {
            Connection = new SQLiteConnection(path);
        }

        public static DBHelper GetInstance(string path = "")
        {
            
            if (_Instance == null){
                _Instance = new DBHelper(path);
            }
            return _Instance;
            
        }

        public SQLiteConnection GetDBConnection()
        {
            return Connection;
        }


    }
}
