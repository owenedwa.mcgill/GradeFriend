﻿using SQLite;
using System;
using System.Collections.Generic;
using SQLiteNetExtensions.Attributes;

namespace GradeFriend
{
    [Table("Student")]
    public class Student
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string StudentId { get; set; }
        public string Name { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Course> Courses { get; set; }
    }
}
