﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace GradeFriend
{
    public class StudentHelper
    {
        SQLiteConnection Connection;

        public StudentHelper(SQLiteConnection connection)
        {
            Connection = connection;
            FlushTable();
            Connection.CreateTable<Student>();
        }

        public bool FlushTable()
        {
            try
            {
                Connection.Execute("delete from " + CONSTANTS.TABLE_STUDENT);
                Connection.Execute("delete from sqlite_sequence where name = '" + CONSTANTS.TABLE_STUDENT + "'");
            }
            catch (SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public bool AddStudent(string name, string studentId, List<Course> courses)
        {
            try
            {
                Student _student = new Student()
                {
                    Name = name,
                    StudentId = studentId,
                    Courses = courses
                };

                Connection.InsertWithChildren(_student, true);
            }
            catch (SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public bool AddStudents(List<Student> students)
        {
            try
            {
                Connection.InsertAllWithChildren(students, true);   
            }
            catch(SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public Student GetStudent(string studentId) {
            Student _student = Connection.Table<Student>().Where(v => v.StudentId.Equals(studentId)).First();
            Connection.GetChildren(_student, true);
            return _student;
        }

        public Student GetStudent(int studentId) {
            return Connection.Get<Student>(studentId);
        }

        public List<Student> GetStudentList()
        {
            return Connection.Table<Student>().ToList();
        }
    }
}
