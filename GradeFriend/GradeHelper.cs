﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace GradeFriend
{
    public class GradeHelper
    {
        SQLiteConnection Connection;

        public GradeHelper(SQLiteConnection connection)
        {
            Connection = connection;
            FlushTable();
            Connection.CreateTable<Grade>();
        }

        public bool FlushTable()
        {
            try
            {
                Connection.Execute("delete from " + CONSTANTS.TABLE_GRADE);
                Connection.Execute("delete from sqlite_sequence where name = '" + CONSTANTS.TABLE_GRADE + "'");
            }
            catch (SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public bool AddGrade(string description, decimal mark, decimal weight, int courseID)
        {
            try
            {
                Grade _grade = new Grade()
                {
                    Description = description,
                    Mark = mark,
                    Weight = weight,
                    CourseID = courseID
                };

                Connection.Insert(_grade);
            }
            catch(SQLiteException e)
            {
                return false;
            }
            return true;
        }

        public List<Grade> GetGrades(Course course)
        {
            var grades = Connection.GetWithChildren<Course>(course.Id, true).Grades;
            return grades;
        }
    }
}
