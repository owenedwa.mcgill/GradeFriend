using Foundation;
using System;
using UIKit;

namespace GradeFriend.iOS
{
    public partial class SettingsViewController : UIViewController
    {

        MyModelIOS myModel;

        public SettingsViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            myModel = MyModelIOS.Instance;

            var plist = NSUserDefaults.StandardUserDefaults;

            var studentId = plist.StringForKey(CONSTANTS.STUDENT_ID);

            if (studentId == null)
            {
                StudentIdField.Text = "";
            } else {
                StudentIdField.Text = studentId;
            }

            ViewModeSelector.SelectedSegment = 0;

        }

        partial void ViewModeChanged(UISegmentedControl sender)
        {
            if (sender.SelectedSegment == 1)
            {
                myModel.GradeWeight = true;
            }
            else
            {
                myModel.GradeWeight = false;
            }
        }

        partial void StudentIdEndEditing(UITextField sender)
        {
            myModel.StudentId = sender.Text;
            var plist = NSUserDefaults.StandardUserDefaults;
            plist.SetString(sender.Text, CONSTANTS.STUDENT_ID);
        }
    }
}