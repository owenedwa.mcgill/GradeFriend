﻿using System;
using System.IO;

namespace GradeFriend.iOS
{
    public class MyModelIOS:MyModel
    {

        private static MyModelIOS _Instance;

        public bool GradeWeight { get; set; }

        private static string Connection {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), CONSTANTS.DATABASE_NAME);
            }
        }

        private MyModelIOS():base(Connection)
        {
            GradeWeight = false;
        }

        public static MyModelIOS Instance
        {
            get
            {
                return _Instance ?? (_Instance = new MyModelIOS());
            }
        }
    }
}
