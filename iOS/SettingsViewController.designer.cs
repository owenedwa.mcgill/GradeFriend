// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace GradeFriend.iOS
{
    [Register ("SettingsViewController")]
    partial class SettingsViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField StudentIdField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl ViewModeSelector { get; set; }

        [Action ("StudentIdEndEditing:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void StudentIdEndEditing (UIKit.UITextField sender);

        [Action ("ViewModeChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ViewModeChanged (UIKit.UISegmentedControl sender);

        void ReleaseDesignerOutlets ()
        {
            if (StudentIdField != null) {
                StudentIdField.Dispose ();
                StudentIdField = null;
            }

            if (ViewModeSelector != null) {
                ViewModeSelector.Dispose ();
                ViewModeSelector = null;
            }
        }
    }
}