using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

namespace GradeFriend.iOS
{
    public partial class CourseViewController : UITableViewController
    {

        MyModelIOS myModel;
        List<Grade> grades;

        string AverageStringTemplate;
        string CourseStringTemplate;

        public CourseViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            myModel = MyModelIOS.Instance;

            AverageStringTemplate = "Your average for this course is ";
            CourseStringTemplate = "Your grades for the course ";

            DoneButton.Clicked += delegate {
                DismissViewController(true, null);
            };
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            grades = myModel.GetGrades(myModel.SelectedCourse);

            decimal _markSum = 0;
            decimal _weightSum = 0;

            foreach(Grade grade in grades)
            {
                _markSum += grade.Mark * grade.Weight;
                _weightSum += grade.Weight;
            }

            var avg = _markSum / _weightSum;

            NavigationItem.Title = CourseStringTemplate + myModel.SelectedCourse.Name + " are:";

            AverageLabel.Text = AverageStringTemplate + avg.ToString();
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            return grades.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (myModel.GradeWeight)
            {
                string cellIdentifier = "GradeWeightCell";
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier, indexPath) as UITableViewCell;

                cell.TextLabel.Text = grades[indexPath.Row].Mark.ToString();
                cell.DetailTextLabel.Text = grades[indexPath.Row].Weight.ToString();

                return cell;
            } else {
                string cellIdentifier = "GradeCell";
                UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier, indexPath) as UITableViewCell;

                cell.TextLabel.Text = grades[indexPath.Row].Mark.ToString();

                return cell;
            }
        }

        public override void AccessoryButtonTapped(UITableView tableView, NSIndexPath indexPath)
        {

            Grade grade = grades[indexPath.Row];

            string detail = grade.Description;

            string title = "Mark: " + grade.Mark + " - Weight: " + grade.Weight;


            UIAlertController alert = UIAlertController.Create(title, detail, UIAlertControllerStyle.Alert);
            alert.AddAction(UIAlertAction.Create("Close", UIAlertActionStyle.Cancel, null));
            PresentViewController(alert, true, null);
            tableView.DeselectRow(indexPath, true);
        }
    }
}