using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GradeFriend.iOS
{
    public partial class CourseListController : UITableViewController
    {

        MyModelIOS myModel;
        List<Course> courses;

        public CourseListController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            myModel = MyModelIOS.Instance;

            var plist = NSUserDefaults.StandardUserDefaults;

            var studentId = plist.StringForKey(CONSTANTS.STUDENT_ID);

            UpdateButton.Clicked += Update;

            if (studentId == null)
            {
                myModel.StudentId = "";
                UIAlertController alert = UIAlertController.Create("No Student Id",
                                                                  "Please ask your administrator for your unique student id.\n Then set it in the settings.",
                                                                   UIAlertControllerStyle.Alert);
                alert.AddAction(UIAlertAction.Create("Close", UIAlertActionStyle.Default, null));
            }
            else
            {
                myModel.StudentId = studentId;
                courses = myModel.ListCourse;
            }
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            return myModel.ListCourse.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            string cellIdentifier = "StudentCourseCell";
            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier, indexPath) as UITableViewCell;

            cell.TextLabel.Text = courses[indexPath.Row].Name;
            return cell;
        }

        public override void AccessoryButtonTapped(UITableView tableView, NSIndexPath indexPath)
        {
            UIAlertController alert = UIAlertController.Create("Infos", courses[indexPath.Row].Teacher, UIAlertControllerStyle.Alert);
            alert.AddAction(UIAlertAction.Create("Close", UIAlertActionStyle.Cancel, null));
            PresentViewController(alert, true, null);
            tableView.DeselectRow(indexPath, true);
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            myModel.SelectedCourse = courses[indexPath.Row];
            tableView.DeselectRow(indexPath, true);
        }

        public async void Update(object sender, EventArgs e)
        {
            Task<bool> updateModelTask = myModel.GetModelDataTask();

            bool result = await updateModelTask;

            if (result)
            {
                courses = myModel.ListCourse;
                TableView.ReloadData();
            }
        }

        public override void Unwind(UIStoryboardSegue unwindSegue, UIViewController subsequentVC)
        {
            base.Unwind(unwindSegue, subsequentVC);
        }
    }
}