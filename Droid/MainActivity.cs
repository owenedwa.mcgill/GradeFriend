﻿using System;  
using Android.App;  
using Android.Content;  
using Android.Runtime;  
using Android.Views;  
using Android.Widget;  
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using Android.Content.Res;
using System.Threading.Tasks;

namespace GradeFriend.Droid
{
    [Activity]
    public class MainActivity : AppCompatActivity
    {
        
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        FrameLayout fragmentContainer;

        MyModelDroid myModel;

        IMenuItem previousItem;  
        Android.Support.V7.App.ActionBarDrawerToggle toggle; 


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            fragmentContainer = FindViewById<FrameLayout>(Resource.Id.fragmentContainer);

            drawerLayout.DrawerClosed += DrawerLayout_DrawerClosed;
            drawerLayout.DrawerOpened += DrawerLayout_DrawerOpened;

            navigationView.SetCheckedItem(0);

            toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                Resource.String.openDrawer,
                Resource.String.closeDrawer
            );

            drawerLayout.AddDrawerListener(toggle);

            navigationView.NavigationItemSelected += (sender, e) =>
            {

                if (previousItem != null)
                    previousItem.SetChecked(false);

                navigationView.SetCheckedItem(e.MenuItem.ItemId);

                previousItem = e.MenuItem;

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.nav_course_list:
                        ListItemClicked(0);
                        break;
                    case Resource.Id.nav_settings:
                        ListItemClicked(1);
                        break;
                }

                drawerLayout.CloseDrawers();
            };

            var fragmentManager = this.FragmentManager;

            var courseList = new CourseListFragment();


            var ft = fragmentManager.BeginTransaction();
            ft.Replace(Resource.Id.fragmentContainer, courseList);
            ft.SetTransition((int)FragmentTransit.None);
            ft.Commit();

            myModel = MyModelDroid.Instance;
        }

        public override void OnBackPressed()
        {
            if (drawerLayout.IsDrawerOpen((int)GravityFlags.Start))
            {
                drawerLayout.CloseDrawer((int)GravityFlags.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (drawerLayout.IsDrawerOpen((int)GravityFlags.Start))
                    {
                        drawerLayout.CloseDrawer(GravityCompat.Start);
                    }
                    else
                    {
                        drawerLayout.OpenDrawer(GravityCompat.Start);
                    }

                    return true;

                case Resource.Id.bar_refresh:
                    Toast.MakeText(this.BaseContext, "Refresh", ToastLength.Long).Show();
                    Update();
                    return true;
            }
            return true;
        }

        private void ListItemClicked(int position)
        {
            Fragment fragment = null;
            switch (position)
            {
                case 0:
                    fragment = new CourseListFragment();
                    break;
                case 1:
                    fragment = new SettingsFragment();
                    break;
            }
            if (fragment != null)
            {
                FragmentManager.BeginTransaction().Replace(Resource.Id.fragmentContainer, fragment, "CourseListFragment").Commit();
            }
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {

            base.OnPostCreate(savedInstanceState);
            toggle.SyncState();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            toggle.OnConfigurationChanged(newConfig);
        }

        private void DrawerLayout_DrawerClosed(object sender, DrawerLayout.DrawerClosedEventArgs e)
        {
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
        }

        private void DrawerLayout_DrawerOpened(object sender, DrawerLayout.DrawerOpenedEventArgs e)
        {
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back);
        }

        public void Update()
        {
            ISharedPreferences prefs = GetSharedPreferences("GradeFriend", FileCreationMode.Private);
            myModel.StudentId = prefs.GetString(CONSTANTS.STUDENT_ID, "");

            var courseList = (CourseListFragment)FragmentManager.FindFragmentByTag("CourseListFragment");
            courseList.UpdateCourseList();
        }
    }
}

