﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GradeFriend.Droid
{
    public class CourseViewFragment : Fragment
    {
        MyModelDroid myModel;

        Course SelectedCourse;

        ListView listView;

        GradeAdapter adapter;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            myModel = MyModelDroid.Instance;

            SelectedCourse = myModel.SelectedCourse;

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            View view = inflater.Inflate(Resource.Layout.CourseViewLayout, container, false);

            listView = view.FindViewById<ListView>(Resource.Id.gradeListView);

            TextView avgView = view.FindViewById<TextView>(Resource.Id.gradeAverageView);

            decimal _markSum = 0;
            decimal _weightSum = 0;

            foreach (Grade grade in SelectedCourse.Grades)
            {
                _markSum += grade.Mark * grade.Weight;
                _weightSum += grade.Weight;
            }

            var avg = _markSum / _weightSum;

            avgView.Text = "Your average for this course is " + avg.ToString();

            adapter = new GradeAdapter(this);

            listView.Adapter = adapter;
            listView.ItemLongClick += ListItemLongClicked;

            return view;
        }

        public void ListItemLongClicked(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            var grade = SelectedCourse.Grades[e.Position];

            AlertDialog.Builder alert = new AlertDialog.Builder(this.Activity);
            alert.SetTitle(grade.Mark + " - " + grade.Weight);
            alert.SetMessage(grade.Description);

            alert.SetNeutralButton("Close", (object s, DialogClickEventArgs ea) => {

            });


            Dialog dialog = alert.Create();
            dialog.Show();
        }
    }
}
