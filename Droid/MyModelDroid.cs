﻿using System;
using System.IO;

namespace GradeFriend.Droid
{
    public class MyModelDroid : MyModel
    {

        private static MyModelDroid _Instance;

        public bool GradeWeight { get; set; }

        private static string Connection
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), CONSTANTS.DATABASE_NAME);
            }
        }

        private MyModelDroid():base(Connection)
        {
            GradeWeight = false;
        }

        public static MyModelDroid Instance
        {
            get
            {
                return _Instance ?? (_Instance = new MyModelDroid());
            }
        }
    }
}
