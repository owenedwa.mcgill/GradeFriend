﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GradeFriend.Droid
{
    public class SettingsFragment : Fragment
    {

        ToggleButton viewModeToggle;
        EditText studentId;
        MyModelDroid myModel;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            myModel = MyModelDroid.Instance;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View view = inflater.Inflate(Resource.Layout.SettingsLayout, container, false);

            viewModeToggle = view.FindViewById<ToggleButton>(Resource.Id.toggleViewMode);
            studentId = view.FindViewById<EditText>(Resource.Id.studentId);

            viewModeToggle.Checked = MyModelDroid.Instance.GradeWeight;

            ISharedPreferences prefs = this.Context.GetSharedPreferences("GradeFriend", FileCreationMode.Private);
            studentId.Text = prefs.GetString(CONSTANTS.STUDENT_ID, "");

            //studentId.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
            //{
            //    Toast.MakeText(this.Activity, e.Text.ToString(), ToastLength.Long).Show();
            //};

            studentId.FocusChange += (object sender, View.FocusChangeEventArgs e) =>
            {
                if (!e.HasFocus)
                {
                    //Toast.MakeText(this.Activity, ((EditText)sender).Text, ToastLength.Long).Show();
                    var editor = prefs.Edit();
                    var studentID = ((EditText)(sender)).Text;
                    editor.PutString(CONSTANTS.STUDENT_ID, studentID);
                    editor.Commit();
                }
            };

            viewModeToggle.CheckedChange += (object sender, CompoundButton.CheckedChangeEventArgs e) =>
            {
                //Toast.MakeText(this.Activity, e.ToString(), ToastLength.Long).Show();
                myModel.GradeWeight = e.IsChecked;
            };

            return view;
        }
    }
}
