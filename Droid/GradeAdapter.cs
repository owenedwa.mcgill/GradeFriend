﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace GradeFriend.Droid
{
    public class GradeAdapter : BaseAdapter
    {
        Activity _activity;
        Course course;
        MyModelDroid myModel;

        public GradeAdapter(Fragment context)
        {
            myModel = MyModelDroid.Instance;
            course = myModel.SelectedCourse;
            _activity = context.Activity;
        }

        public override int Count => course.Grades.Count;

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return course.Grades[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view;
            if (myModel.GradeWeight)
            {
                view = convertView ?? _activity.LayoutInflater.Inflate(
                    Resource.Layout.GradeWeightListItem, parent, false);
                var gradeValue = view.FindViewById<TextView>(Resource.Id.gradeValue);
                var gradeWeight = view.FindViewById<TextView>(Resource.Id.gradeWeight);
                gradeValue.Text = course.Grades[position].Mark.ToString();
                gradeWeight.Text = course.Grades[position].Weight.ToString();
            } else {
                view = convertView ?? _activity.LayoutInflater.Inflate(
                    Resource.Layout.GradeListItem, parent, false);
                var gradeValue = view.FindViewById<TextView>(Resource.Id.gradeValue);
                gradeValue.Text = course.Grades[position].Mark.ToString();
            }


            return view;

        }
    }
}
