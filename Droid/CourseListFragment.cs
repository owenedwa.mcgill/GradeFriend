﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GradeFriend.Droid
{
    public class CourseListFragment : Fragment
    {

        MyModelDroid myModel;
        CourseAdapter adapter;
        ListView listView;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetHasOptionsMenu(true);

            myModel = MyModelDroid.Instance;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            View view = inflater.Inflate(Resource.Layout.CourseListLayout, container, false);

            listView = view.FindViewById<ListView>(Resource.Id.courseListView);

            adapter = new CourseAdapter(this);

            listView.Adapter = adapter;
            listView.ItemClick += ListItemClicked;
            listView.ItemLongClick += ListItemLongClicked;

            ISharedPreferences prefs = this.Context.GetSharedPreferences("GradeFriend", FileCreationMode.Private);
            myModel.StudentId = prefs.GetString(CONSTANTS.STUDENT_ID, "");

            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.bar_menu, menu);
        }

        public void ListItemClicked(object sender, AdapterView.ItemClickEventArgs e) {
            myModel.SelectedCourse = myModel.ListCourse[e.Position];
            //Toast.MakeText(this.Activity, myModel.SelectedCourse.Name, ToastLength.Short).Show();
            var ft = FragmentManager.BeginTransaction();
            var courseView = new CourseViewFragment();
            ft.AddToBackStack(null);
            ft.Replace(Resource.Id.fragmentContainer, courseView);
            ft.Commit();
        }

        public void ListItemLongClicked(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            var course = myModel.ListCourse[e.Position];

            AlertDialog.Builder alert = new AlertDialog.Builder(this.Activity);
            alert.SetTitle(course.Name);
            alert.SetMessage(course.Teacher);

            alert.SetNeutralButton("Close", (object s, DialogClickEventArgs ea) => {
                
            });


            Dialog dialog = alert.Create();
            dialog.Show();
        }

        public async void UpdateCourseList() {
            
            Task<bool> updateModelTask = myModel.GetModelDataTask();

            bool result = await updateModelTask;

            if (result)
            {
                adapter.SetCourseList();
                adapter.NotifyDataSetChanged();
            }
        }
    }
}
