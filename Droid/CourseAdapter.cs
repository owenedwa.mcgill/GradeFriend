﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace GradeFriend.Droid
{
    public class CourseAdapter : BaseAdapter
    {
        Activity _activity;
        List<Course> courseList;
        MyModelDroid myModel;

        public CourseAdapter(Fragment context)
        {
            myModel = MyModelDroid.Instance;
            courseList = myModel.ListCourse;
            _activity = context.Activity;
        }

        public override int Count => courseList.Count;

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return courseList[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var view = convertView ?? _activity.LayoutInflater.Inflate(
       Resource.Layout.CourseListItem, parent, false);
            var courseName = view.FindViewById<TextView>(Resource.Id.CourseName);
            courseName.Text = courseList[position].Name;

            return view;

        }

        public void SetCourseList()
        {
            courseList = myModel.ListCourse;
        }
    }
}
