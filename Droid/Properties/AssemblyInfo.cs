﻿using System.Reflection;
using System.Runtime.CompilerServices;
using Android.App;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle("GradeFriend.Droid")]
[assembly: Application(AllowBackup = true,
                       Label = "GradeFriend",
                       HardwareAccelerated = true,
                       Theme = "@style/GradeFriendTheme")]
